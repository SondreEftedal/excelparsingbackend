﻿using Microsoft.EntityFrameworkCore;
using RidelBackend.Models;


namespace RidelBackend.Data
{
    public class ExcelDataDbContext : DbContext
    {
        public ExcelDataDbContext(DbContextOptions options) : base(options) 
        {

        }
        
        public DbSet<ExcelData> ExcelData {  get; set; }
    }
}
