﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RidelBackend.Models
{
    public class ExcelData
    {
        
        public int Id { get; set; }
        public DateTime Tidspunkt { get; set; }
        public decimal KundePris { get; set; }
        public decimal KundePrisEksMva { get; set; }
        public decimal Refundert { get; set; }
        public string BetalingsMåte { get; set; }
        public string BookingNummer { get; set; }
        public string Løyve { get; set; }
        public string OrganisasjonsNummer { get; set; }
        public string LøyveHaver { get; set; }
        public string SjåførNummer { get; set; }
        public string Sjåfør { get; set; }
        public string Status { get; set; }
        public decimal MvaSats { get; set; }
       


    }
}
