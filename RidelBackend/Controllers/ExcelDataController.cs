﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RidelBackend.Models;
using RidelBackend.Services;
using System.Net.Mime;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RidelBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class ExcelDataController : ControllerBase
    {
        private readonly IExcelDataService _excelDataService;

        public ExcelDataController(IExcelDataService excelDataService)
        {
            _excelDataService = excelDataService;
        }

        /// <summary>
        /// Returns all the Excel data from the database.
        /// </summary>
        /// <returns></returns>
        // GET: api/<DataController>
        [HttpGet]
        public async Task<IEnumerable<ExcelData>> GetAllExcelData()
        {
            return await _excelDataService.GetAllExcelDataAsync();
        }
        /// <summary>
        /// Returns spesific data from the database with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/<DataController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ExcelData>> GetExcelData(int id)
        {
            ExcelData data = await _excelDataService.GetSpesificExcelDataAsync(id);
            if(data == null)
            {
                return NotFound();
            }
            return data;
            
        }
        /// <summary>
        /// Adds new Excel data to the database
        /// </summary>
        /// <param name="value"></param>
        // POST api/<DataController>
        [HttpPost]
        public async Task<ActionResult<ExcelData>> PostExcelData(ExcelData data)
        {
            data = await _excelDataService.AddExcelDataAsync(data);
            return CreatedAtAction(nameof(GetExcelData), new {id = data.Id}, data);
        }
        
        /// <summary>
        /// Updates Excel data. Must pass data and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        // PUT api/<DataController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExcelData(int id, ExcelData data)
        {
            if (data == null)
            {
                return BadRequest("Invalid data. Please provide valid ExcelData object.");
            }

            if (id != data.Id)
            {
                return BadRequest("Invalid ID. The ID in the route parameter must match the ID in the data object.");
            }

            if (!_excelDataService.ExcelDataExists(id))
            {
                return NotFound("Excel data with the specified ID not found.");
            }
            await _excelDataService.UpdateExcelDataAsync(data);
            return NoContent();
        }
        /// <summary>
        /// Deletes Excel data from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<DataController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExcelData(int id)
        {
            if (!_excelDataService.ExcelDataExists(id))
            {
                return NotFound();
            }
            await _excelDataService.DeleteExcelDataAsync(id);

            return NoContent();
        }
    }
}
