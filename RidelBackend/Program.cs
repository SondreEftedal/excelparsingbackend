using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using RidelBackend.Data;
using RidelBackend.Services;
using System.Reflection;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Intervjurunde 2 API",
                Description = "Et enkelt ASP.NET API brukt for � demonstrere caseoppgaven gitt i forkant av intervjurunde 2.",
                TermsOfService = new Uri("https://example.com/terms"),
                Contact = new OpenApiContact
                {
                    Name = "Sondre Eftedal",
                    Email = "Sondre-eftedal@hotmail.com",
                    Url = new Uri("https://no.linkedin.com/in/sondre-eftedal-1649b693"),
                },
                License = new OpenApiLicense
                {
                    Name = "Use under LICX",
                    Url = new Uri("https://example.com/license"),
                }
            });
            // Set the comments path for the Swagger JSON and UI.
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);

        });
        builder.Services.AddCors(policyBuilder =>
                policyBuilder.AddDefaultPolicy(policy =>
                    policy.WithOrigins("*").AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin())
            );

        builder.Services.AddScoped(typeof(IExcelDataService), typeof(ExcelDataService));


        //Connect to SQL database.
        builder.Services.AddDbContext<ExcelDataDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {

        }
        app.UseSwagger();
        app.UseSwaggerUI();
        app.UseHttpsRedirection();
        app.UseCors();



        app.UseAuthorization();

        app.MapControllers();

        app.Run();
    }
}