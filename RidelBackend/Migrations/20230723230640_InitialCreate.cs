﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RidelBackend.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExcelData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tidspunkt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    KundePris = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    KundePrisEksMva = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Refundert = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    BetalingsMåte = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BookingNummer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Løyve = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganisasjonsNummer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LøyveHaver = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SjåførNummer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Sjåfør = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MvaSats = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExcelData", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExcelData");
        }
    }
}
