﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RidelBackend.Data;
using RidelBackend.Models;

namespace RidelBackend.Services
{
    public class ExcelDataService : IExcelDataService
    {

        private readonly ExcelDataDbContext _context;
        public ExcelDataService(ExcelDataDbContext context)
        {
            _context = context;
        }
        public async Task<ExcelData> AddExcelDataAsync(ExcelData data)
        {
            _context.ExcelData.Add(data);
            await _context.SaveChangesAsync();
            return data;
        }

        public async Task DeleteExcelDataAsync(int id)
        {
            var data = await _context.ExcelData.FindAsync(id);
            _context.ExcelData.Remove(data);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.ExcelData.Any(x => x.Id == id);
        }

        public async Task<IEnumerable<ExcelData>> GetAllExcelDataAsync()
        {
            return await _context.ExcelData.ToListAsync();
        }

        public async Task<ExcelData> GetSpesificExcelData(int id)
        {
            return await _context.ExcelData.FindAsync(id);
        }

        public async Task UpdateExcelDataAsync(ExcelData data)
        {
            _context.Entry(data).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool ExcelDataExists(int id)
        {
            return _context.ExcelData.Any(x =>x.Id == id);
        }

        public async Task<ExcelData> GetSpesificExcelDataAsync(int id)
        {
            return await _context.ExcelData.FindAsync(id);

        }
    }
}
