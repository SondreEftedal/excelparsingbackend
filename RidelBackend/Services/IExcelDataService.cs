﻿using RidelBackend.Models;

namespace RidelBackend.Services
{
    public interface IExcelDataService
    {
        public Task<ExcelData> GetSpesificExcelDataAsync(int id);
        public Task<IEnumerable<ExcelData>> GetAllExcelDataAsync();
        public Task<ExcelData> AddExcelDataAsync(ExcelData data);
        public bool ExcelDataExists(int id);
        public Task DeleteExcelDataAsync(int id);
        public Task UpdateExcelDataAsync(ExcelData data);


    }
}
